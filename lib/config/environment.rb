# Application wide requirements
require 'active_record'
require 'active_model'
require 'active_support'
require 'multi_json'
require 'sqlite3'
require 'yaml'
require 'logger'

# Set the ENV which defaults to 'development'. Also affects which database to create and connect to.
ENV['RAILS_ENV'] ||= 'development'

# This section is for development and testing. Load your testing framework(s) require's here
case ENV['RAILS_ENV']
when 'development', 'test'
  require 'rspec'
  require 'turnip'
  require 'pry'
  require 'pry-nav'
  require 'pry-coolline'
  require 'pry-doc'
  require 'pry-stack_explorer'
  require 'pry-exception_explorer'
  require 'pryable'
  require 'pry-git'
  require 'pry-editline'
  require 'pry-highlight'
  require 'pry-buffers'
  require 'pry-developer_tools'
  require 'pry-syntax-hacks'
else
  true
end

# Load the db config and create a connectoid. Make an ivar so its shared throughout the application
@dbconfig = YAML::load(File.open(File.join(File.dirname(__FILE__), '../../db/config.yml')))[ENV['RAILS_ENV']]

# Create an ActiveRecord logger
#ActiveRecord::Base.logger = Logger.new(STDERR) # Simple logging utility -- standard lib
# Uncomment out the above and comment this one if you'd rather the console display than a logfile
ActiveRecord::Base.logger = Logger.new("#{@APP_ROOT}/logs/#{ENV['RAILS_ENV']}.log") # Simple logging

# Establish the database connection
ActiveRecord::Base.establish_connection(@dbconfig) # Line that actually connects the db.
