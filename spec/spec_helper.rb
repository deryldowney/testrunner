# Set base environment
ENV['RAILS_ENV'] = 'development'

# spec_helper should require 'rspec' regardless
require 'rspec'
require 'turnip'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[File.join(File.dirname(__FILE__), "support/**/*.rb")].each {|f| require f}

Turnip::Config.step_dirs = ['spec/step_definitions']

